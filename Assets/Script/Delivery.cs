using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delivery : MonoBehaviour
{
    [SerializeField]
    public float secondsToDelete = 1f;
    [SerializeField]
    public Color32 hasPackageColor = new Color32(1, 1, 1, 1);
    [SerializeField]
    public Color32 noPackageColor = new Color32(1, 0, 0, 1);

    private bool hasPackage = false;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Crashed!");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Package" && !hasPackage)
        {
            hasPackage = true;
            spriteRenderer.color = hasPackageColor;
            Debug.Log("Package Picked Up!");
            Destroy(collision.gameObject, secondsToDelete);
        }

        if (collision.tag == "Customer" && hasPackage)
        {
            hasPackage = false;
            spriteRenderer.color = noPackageColor;
            Debug.Log("Package Delivered To Customer!");
        }

    }
}
